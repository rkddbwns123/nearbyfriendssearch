package shop.usehwa.gen.api.member;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import shop.usehwa.gen.api.member.entity.MemberInfo;
import shop.usehwa.gen.api.member.enums.Gender;
import shop.usehwa.gen.api.member.repository.MemberInfoRepository;

import java.util.List;

@SpringBootTest
public class ApiMemberApplicationTests {
    @Autowired
    MemberInfoRepository memberInfoRepository;
    @Test
    void contextLoads() {

    }
    @Test
    void dataTest1() {
        double searchX = 126.835673;
        double searchY = 37.317927;
        double searchDistance = 1000;
        String gender = Gender.MAN.toString();

        List<MemberInfo> result = memberInfoRepository.findAllByNearFriends(searchY, searchX, searchDistance, gender);

        int test = 1;
    }

}
