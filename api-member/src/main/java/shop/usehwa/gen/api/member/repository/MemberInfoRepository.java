package shop.usehwa.gen.api.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import shop.usehwa.gen.api.member.entity.MemberInfo;

import java.util.List;

public interface MemberInfoRepository extends JpaRepository<MemberInfo, Long> {

    @Query(
            value = "select * from member_info where earth_distance(ll_to_earth(posY, posx), ll_to_earth(:pointY, :pointX)) <= :searchDistance and member_info.gender = :gender",
            nativeQuery = true
    )
    List<MemberInfo> findAllByNearFriends(@Param("pointY") double pointY, @Param("pointX") double pointX, @Param("searchDistance") double searchDistance, @Param("gender") String gender);
}
