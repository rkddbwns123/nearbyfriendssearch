package shop.usehwa.gen.api.member.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.member.enums.Gender;
import shop.usehwa.gen.api.member.model.MemberInfoRequest;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberInfo {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "닉네임")
    @Column(nullable = false, length = 20)
    private String nickName;

    @ApiModelProperty(notes = "취미")
    @Column(nullable = false, length = 50)
    private String hobby;

    @ApiModelProperty(notes = "성별")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Gender gender;

    @ApiModelProperty(notes = "x 좌표값")
    @Column(nullable = false)
    private Double posX;

    @ApiModelProperty(notes = "y 좌표값")
    @Column(nullable = false)
    private Double posY;

    private MemberInfo(MemberInfoBuilder builder) {
        this.nickName = builder.nickName;
        this.hobby = builder.hobby;
        this.gender = builder.gender;
        this.posX = builder.posX;
        this.posY = builder.posY;

    }

    public static class MemberInfoBuilder implements CommonModelBuilder<MemberInfo> {

        private final String nickName;
        private final String hobby;
        private final Gender gender;
        private final Double posX;
        private final Double posY;

        public MemberInfoBuilder(MemberInfoRequest request) {
            this.nickName = request.getNickName();
            this.hobby = request.getHobby();
            this.gender = request.getGender();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
        }

        @Override
        public MemberInfo build() {
            return new MemberInfo(this);
        }
    }
}
