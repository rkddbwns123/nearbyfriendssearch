package shop.usehwa.gen.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.usehwa.gen.api.member.model.MemberInfoRequest;
import shop.usehwa.gen.api.member.model.NearFriendsItem;
import shop.usehwa.gen.api.member.model.NearFriendsSearchRequest;
import shop.usehwa.gen.api.member.service.MemberInfoService;
import shop.usehwa.gen.common.response.model.CommonResult;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member-info")
public class MemberInfoController {
    private final MemberInfoService memberInfoService;

    @ApiOperation(value = "회원 등록")
    @PostMapping("/new")
    public CommonResult setMemberInfo(@RequestBody @Valid MemberInfoRequest request) {
        memberInfoService.setMemberInfo(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "주변 친구 찾기")
    @PostMapping("/distance")
    public ListResult<NearFriendsItem> getNearFriends(@RequestBody @Valid NearFriendsSearchRequest request) {
        return ResponseService.getListResult(memberInfoService.getNearFriends(request.getPosY(), request.getPosX(), request.getDistance()), true);
    }
}
