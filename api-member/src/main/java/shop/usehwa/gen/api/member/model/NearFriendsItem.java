package shop.usehwa.gen.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.usehwa.gen.api.member.enums.Gender;
import shop.usehwa.gen.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NearFriendsItem {
    @ApiModelProperty(notes = "닉네임")
    private String nickname;

    @ApiModelProperty(notes = "취미")
    private String hobby;

    @ApiModelProperty(notes = "성별 / enum값")
    private String gender;

    @ApiModelProperty(notes = "성별 / 한글값")
    private String genderName;

    @ApiModelProperty(notes = "거리")
    private Double distance;

    private NearFriendsItem(NearFriendsItemBuilder builder) {
        this.nickname = builder.nickname;
        this.hobby = builder.hobby;
        this.gender = builder.gender;
        this.genderName = builder.genderName;
        this.distance = builder.distance;
    }

    public static class NearFriendsItemBuilder implements CommonModelBuilder<NearFriendsItem> {

        private final String nickname;
        private final String hobby;
        private final String gender;
        private final String genderName;
        private final Double distance;

        public NearFriendsItemBuilder(String nickname, String hobby, String gender, Double distance) {
            this.nickname = nickname;
            this.hobby = hobby;
            this.gender = gender;
            this.genderName = Gender.valueOf(gender).getGenderName();
            this.distance = distance;
        }

        @Override
        public NearFriendsItem build() {
            return new NearFriendsItem(this);
        }
    }
}
