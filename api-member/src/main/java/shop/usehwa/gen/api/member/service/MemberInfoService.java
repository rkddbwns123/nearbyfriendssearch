package shop.usehwa.gen.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.member.entity.MemberInfo;
import shop.usehwa.gen.api.member.model.MemberInfoRequest;
import shop.usehwa.gen.api.member.model.NearFriendsItem;
import shop.usehwa.gen.api.member.repository.MemberInfoRepository;
import shop.usehwa.gen.common.response.model.ListResult;
import shop.usehwa.gen.common.response.service.ListConvertService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberInfoService {

    @PersistenceContext
    EntityManager entityManager;

    private final MemberInfoRepository memberInfoRepository;

    public void setMemberInfo(MemberInfoRequest request) {
        MemberInfo addData = new MemberInfo.MemberInfoBuilder(request).build();

        memberInfoRepository.save(addData);
    }

    public ListResult<NearFriendsItem> getNearFriends(double posY, double posX, int distance) {
        double distanceResult = distance * 1000;

        String queryString = "select * from public.get_near_friends(" + posX + ", " + posY + ", " + distanceResult + ")";
        Query nativeQuery = entityManager.createNativeQuery(queryString);
        List<Object[]> resultList = nativeQuery.getResultList();

        List<NearFriendsItem> result = new LinkedList<>();
        for (Object[] resultItem : resultList) {
            result.add(
                    new NearFriendsItem.NearFriendsItemBuilder(
                            resultItem[0].toString(),
                            resultItem[1].toString(),
                            resultItem[2].toString(),
                            Double.parseDouble(resultItem[3].toString())).build()
            );
        }

        return ListConvertService.settingResult(result);
    }
}
