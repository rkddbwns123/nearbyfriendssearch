package shop.usehwa.gen.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class NearFriendsSearchRequest {

    @ApiModelProperty(notes = "내 위치 Y좌표 값")
    @NotNull
    private Double posY;

    @ApiModelProperty(notes = "내 위치 X좌표 값")
    @NotNull
    private Double posX;

    @ApiModelProperty(notes = "내 주변 조회할 거리")
    @NotNull
    private Integer distance;
}
