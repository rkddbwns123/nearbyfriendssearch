package shop.usehwa.gen.api.member.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import shop.usehwa.gen.api.member.enums.Gender;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberInfoRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String nickName;

    @NotNull
    @Length(min = 1, max = 50)
    private String hobby;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @NotNull
    @Min(value = 0)
    private Double posX;

    @NotNull
    @Min(value = 0)
    private Double posY;
}
