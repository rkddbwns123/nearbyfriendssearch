package shop.usehwa.gen.api.kakao.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import shop.usehwa.gen.api.kakao.model.RegionToAddressResponse;
import shop.usehwa.gen.api.kakao.model.SearchAddressResponse;
import shop.usehwa.gen.api.kakao.model.SearchRegionResponse;
import shop.usehwa.gen.api.kakao.service.GeoService;
import shop.usehwa.gen.common.response.model.SingleResult;
import shop.usehwa.gen.common.response.service.ResponseService;

@Api(tags = "좌표 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/geo")
public class GeoController {
    private final GeoService geoService;

    @ApiOperation(value = "주소로 좌표 변환하기")
    @GetMapping("/search/address")
    public SingleResult<SearchAddressResponse> getSearchAddress(@RequestParam("searchAddress") String searchAddress) {
        return ResponseService.getSingleResult(geoService.getSearchAddress(searchAddress));
    }
    @ApiOperation(value = "좌표로 행정 구역 받기")
    @GetMapping("/search/region")
    public SingleResult<SearchRegionResponse> getSearchRegion(@RequestParam("x") String x, @RequestParam("y") String y) {
        return ResponseService.getSingleResult(geoService.getSearchRegion(x, y));
    }
    @ApiOperation(value = "좌표로 주소 변환하기")
    @GetMapping("/region-to/address")
    public SingleResult<RegionToAddressResponse> getRegionToAddress(@RequestParam("x") String x, @RequestParam("y") String y) {
        return ResponseService.getSingleResult(geoService.getRegionToAddress(x, y));
    }
}
