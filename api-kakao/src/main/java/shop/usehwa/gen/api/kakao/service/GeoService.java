package shop.usehwa.gen.api.kakao.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import shop.usehwa.gen.api.kakao.enums.KakaoUri;
import shop.usehwa.gen.api.kakao.lib.RestApi;
import shop.usehwa.gen.api.kakao.model.RegionToAddressResponse;
import shop.usehwa.gen.api.kakao.model.SearchAddressResponse;
import shop.usehwa.gen.api.kakao.model.SearchRegionResponse;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Service
public class GeoService {
    @Value("${kakao.api.domain}")
    String KAKAO_API_DOMAIN;
    @Value("${kakao.api.rest-key}")
    String KAKAO_API_REST_KEY;

    public SearchAddressResponse getSearchAddress(String addressValue) {
        String apiFullUri = KAKAO_API_DOMAIN + KakaoUri.SEARCH_ADDRESS.getApiSubUri();

        String queryString = "?query=" + URLEncoder.encode(addressValue, StandardCharsets.UTF_8);

        String resultUrl = apiFullUri + queryString;

        return RestApi.callApi(HttpMethod.GET, resultUrl, KAKAO_API_REST_KEY, SearchAddressResponse.class);
    }
    public SearchRegionResponse getSearchRegion(String x, String y) {
        String apiFullUri = KAKAO_API_DOMAIN + KakaoUri.SEARCH_REGION.getApiSubUri();

        String queryString = "?x=" + x + "&y=" + y;

        String resultUrl = apiFullUri + queryString;

        return RestApi.callApi(HttpMethod.GET, resultUrl, KAKAO_API_REST_KEY, SearchRegionResponse.class);
    }

    public RegionToAddressResponse getRegionToAddress(String x, String y) {
        String apiFullUri = KAKAO_API_DOMAIN + KakaoUri.REGION_TO_ADDRESS.getApiSubUri();

        String queryString = "?x=" + x + "&y=" + y + "&input_coord=WGS84";

        String resultUrl = apiFullUri + queryString;

        return RestApi.callApi(HttpMethod.GET, resultUrl, KAKAO_API_REST_KEY, RegionToAddressResponse.class);
    }
}
