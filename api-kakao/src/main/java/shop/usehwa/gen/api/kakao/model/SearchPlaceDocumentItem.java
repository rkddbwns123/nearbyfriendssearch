package shop.usehwa.gen.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchPlaceDocumentItem {
    private String address_name;

    private String category_name;

    private String place_name;

    private String phone;

    private String place_url;
}
