package shop.usehwa.gen.api.kakao.lib;

import com.google.gson.Gson;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.web.client.RestTemplate;
import shop.usehwa.gen.common.exception.CMissingDataException;

import java.net.URI;

public class RestApi {
    public static <T> T callApi(HttpMethod httpMethod, String apiUrl, String restKey, Class<T> responseModel) {
        try {
            URI uri = URI.create(apiUrl);

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "KakaoAK " + restKey);
            headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
            headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

            RequestEntity<String> requestEntity = new RequestEntity<>(headers, httpMethod, uri);
            String responseText = restTemplate.exchange(requestEntity, String.class).getBody();

            Gson gson = new Gson();

            return gson.fromJson(responseText, responseModel);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CMissingDataException();
        }

    }

}
