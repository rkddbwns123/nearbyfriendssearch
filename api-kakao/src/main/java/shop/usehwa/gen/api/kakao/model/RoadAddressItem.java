package shop.usehwa.gen.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoadAddressItem {
    private String address_name;

    private String building_name;

    private String zone_no;
}
