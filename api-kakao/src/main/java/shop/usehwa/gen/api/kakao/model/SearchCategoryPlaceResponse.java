package shop.usehwa.gen.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SearchCategoryPlaceResponse {
    private List<SearchCategoryPlaceDocumentItem> documents;
}
