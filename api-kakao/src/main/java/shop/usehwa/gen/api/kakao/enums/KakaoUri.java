package shop.usehwa.gen.api.kakao.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KakaoUri {
    SEARCH_ADDRESS("주소 검색하기", "/v2/local/search/address.json"),
    SEARCH_REGION("행정구역정보 검색하기", "/v2/local/geo/coord2regioncode.json"),
    REGION_TO_ADDRESS("좌표로 주소 가져오기","/v2/local/geo/coord2address.json"),
    EXCHANGE_REGION("좌표계 변환하기", "/v2/local/geo/transcoord.json"),
    SEARCH_PLACE("키워드로 장소 검색하기", "/v2/local/search/keyword.json"),
    SEARCH_CATEGORY_PLACE("카테고리로 장소 검색하기", "/v2/local/search/category.json");

    private final String apiName;
    private final String apiSubUri;
}
