package shop.usehwa.gen.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressItem {
    private String address_name;
}
