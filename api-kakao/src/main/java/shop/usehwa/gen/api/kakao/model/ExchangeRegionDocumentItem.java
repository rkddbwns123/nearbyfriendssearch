package shop.usehwa.gen.api.kakao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExchangeRegionDocumentItem {
    private Double x;
    private Double y;
}
