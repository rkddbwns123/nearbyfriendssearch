package shop.usehwa.gen.api.kakao;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import shop.usehwa.gen.api.kakao.enums.KakaoUri;
import shop.usehwa.gen.api.kakao.model.*;

import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@SpringBootTest
public class ApiKakaoApplicationTests {
    @Test
    void contextLoads() {

    }
    @Test
    void apiCallTest() {      //주소로 좌표 변환하기
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_ADDRESS.getApiSubUri();

        String addressValue = "옥구천동로 404";
        String queryString = "?query=" + URLEncoder.encode(addressValue, StandardCharsets.UTF_8);

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 19c00ec45dfe22e2d2585e7739cc4565");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchAddressResponse> responseEntity = restTemplate.exchange(requestEntity, SearchAddressResponse.class);

        SearchAddressResponse result = responseEntity.getBody();

        int test = 1;

    }
    @Test
    void apiCallTest2() { //좌표로 행정구역 정보 받기
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_REGION.getApiSubUri();

        String x = "128.1086228";
        String y = "37.4012191";
        String queryString = "?x=" + x + "&y=" + y;

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 19c00ec45dfe22e2d2585e7739cc4565");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchRegionResponse> responseEntity = restTemplate.exchange(requestEntity, SearchRegionResponse.class);

        SearchRegionResponse result = responseEntity.getBody();

        int test = 1;
    }
    @Test
    void apiCallTest3() { //좌표로 주소 변환하기
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.REGION_TO_ADDRESS.getApiSubUri();

        String x = "127.423084873712";
        String y = "37.0789561558879";

        String queryString = "?x=" + x + "&y=" + y + "&input_coord=WGS84";

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 19c00ec45dfe22e2d2585e7739cc4565");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<RegionToAddressResponse> responseEntity = restTemplate.exchange(requestEntity, RegionToAddressResponse.class);

        RegionToAddressResponse result = responseEntity.getBody();

        int test = 1;
    }
    @Test
    void apiCallTest4() { //좌표계 변환하기
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.EXCHANGE_REGION.getApiSubUri();

        String queryString = "?x=160710.37729270622&y=-4388.879299157299&input_coord=WTM&output_coord=WGS84";

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 19c00ec45dfe22e2d2585e7739cc4565");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<ExchangeRegionResponse> responseEntity = restTemplate.exchange(requestEntity, ExchangeRegionResponse.class);

        ExchangeRegionResponse result = responseEntity.getBody();

        int test = 1;
    }
    @Test
    void apiCallTest5() { //키워드로 장소 검색하기
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_PLACE.getApiSubUri();

        String addressValue = "?y=37.514322572335935&x=127.06283102249932&radius=20000";
        String queryName = "카카오 프렌즈";
        String queryString = addressValue + "&query=" + URLEncoder.encode(queryName, StandardCharsets.UTF_8);

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 19c00ec45dfe22e2d2585e7739cc4565");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchPlaceResponse> responseEntity = restTemplate.exchange(requestEntity, SearchPlaceResponse.class);

        SearchPlaceResponse result = responseEntity.getBody();

        int test = 1;
    }
    @Test
    void apiCallTest6() { //카테고리로 장소 검색하기
        String apiDomain = "https://dapi.kakao.com";
        String apiFullUri = apiDomain + KakaoUri.SEARCH_CATEGORY_PLACE.getApiSubUri();

        String queryString = "?category_group_code=PM9&radius=20000";

        URI uri = URI.create(apiFullUri + queryString);

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.add("Authorization", "KakaoAK 19c00ec45dfe22e2d2585e7739cc4565");
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);

        RequestEntity<String> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, uri);
        ResponseEntity<SearchCategoryPlaceResponse> responseEntity = restTemplate.exchange(requestEntity, SearchCategoryPlaceResponse.class);

        SearchCategoryPlaceResponse result = responseEntity.getBody();

        int test = 1;
    }
}
