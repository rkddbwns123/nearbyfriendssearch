# 동네 친구 찾기 API
    2023.03.06 - 2023.03.10 // 강유준
------------

### 등록된 회원들의 GPS를 이용해 내 주변 특정 거리에 있는 친구들을 찾을 수 있게 하는 API입니다.

------------

#### 사용 기술

    1. Java

    2. JPA

    3. Postgres (Procedure)

------------

#### 프로시저
````
CREATE OR REPLACE FUNCTION public.get_near_friends(positionX DOUBLE PRECISION, positionY DOUBLE PRECISION, distance DOUBLE PRECISION)
RETURNS TABLE
(
    nickname varchar
    , hobby varchar
    , gender varchar
    , distance_m double precision
)
as
$$
DECLARE
    v_record RECORD;
BEGIN
    for v_record in (
        select
            member.nickname, member.hobby, member.gender, earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) as distance_m
        from member
        where earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) <= distance
        order by distance_m asc
    )
    loop
        nickname := v_record.nickname;
        hobby := v_record.hobby;
        gender := v_record.gender;
        distance_m := v_record.distance_m;
        return next;
    end loop;
END;
$$
LANGUAGE plpgsql
````

------------

#### 기능

````
1. Create ( 회원 등록 )

2. Read ( 등록 X,Y값 좌표를 이용하여 주변 특정 거리에 있는 회원들 리스트를 가져오기 )
 / Get 기능이지만 Body가 필요해 PostMapping 사용 /

3. KakaoApi를 이용한 GPS값 변환 (주소 -> 좌표 변환 / 좌표 -> 주소 변환)
````

------------